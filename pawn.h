#ifndef PAWN_H
#define PAWN_H
#include "piece.h"

class Pawn : public Piece {
public:
    Pawn(char color, int* position);
    QString to_string() override;
    void move(Board* board) override;
};

#endif // PAWN_H
