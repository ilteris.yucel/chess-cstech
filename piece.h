#ifndef PIECE_H
#define PIECE_H
//Since the Piece and Board classes include each other,
//we re-declare each other to avoid the compiler error.
class Board;
#include "board.h"
#include <QString>
#include <algorithm>
#include <iostream>

//An abstract class to create different pieces class
//Functions of different behavior is defined as virtual
//and these functions implemented based on their behavior
//to runtime polymorphism.
class Piece {
private:
    int value;
    char color;
    int* position;
    bool marked;
public:
    Piece(int value, char color, int* position);
    virtual ~Piece();
    bool change_color(char color);
    int get_value();
    char get_color();
    int* get_position();
    bool check_marked();
    //If threatened, it will be marked.
    void mark();
    void unmark();
    virtual QString to_string() = 0;
    //It is the function that determines
    //the squares that the pieces can go according to their movement patterns.
    virtual void move(Board* board) = 0;
};

#endif // PIECE_H
