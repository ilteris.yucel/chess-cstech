#include "board.h"
#include <fstream>
#include "pawn.h"
#include "king.h"
#include "knight.h"
#include "bishop.h"
#include "rook.h"
#include "queen.h"

Board::Board(std::string filepath) {
    this->filepath = filepath;
    this->data = new Piece** [8];
    for(int i = 0; i < 8; i++)
        this->data[i] = new Piece*[8];
    std::cout << this->filepath << std::endl;
}
Board::~Board() {
    for(int i = 0; i < 8; i++)
        delete[] this->data[i];
    delete[] this->data;
}
void Board::read_file() {
    char* line = new char[24];
    char* piece;
    const char delimeter[2] = " ";
    std::ifstream board_file;
    board_file.open(this->filepath);
    int* position = new int[2];
    position[0] = 0;
    position[1] = 0;
    int* tmp_pos;
    //The file was read line by line and the strings indicating
    //the stones were detected by dividing the lines with spaces.
    while(board_file.getline(line, 25)){
        piece = strtok(line,delimeter);
        while(piece != nullptr) {
            tmp_pos = new int[2];
            tmp_pos[0] = position[0];
            tmp_pos[1] = position[1];
            switch (piece[0]) {
                case 'p':
                    this->data[position[0]][position[1]] = new Pawn(piece[1], tmp_pos);
                    break;
                case 'a':
                this->data[position[0]][position[1]] = new Knight(piece[1], tmp_pos);
                    break;
                case 's':
                    this->data[position[0]][position[1]] = new King(piece[1], tmp_pos);
                    break;
                case 'f':
                    this->data[position[0]][position[1]] = new Bishop(piece[1], tmp_pos);
                    break;
                case 'k':
                    this->data[position[0]][position[1]] = new Rook(piece[1], tmp_pos);
                    break;
                case 'v':
                    this->data[position[0]][position[1]] = new Queen(piece[1], tmp_pos);
                    break;
                default:
                    break;
            }
            piece = strtok(nullptr,delimeter);
            this->increment_position(position);
        }
    }
    board_file.close();
    delete[] line;

}

void Board::increment_position(int* position){
    //The board is read from left to right from top to bottom.
    //When the end of the board is reached horizontally,
    //a vertical bottom line is reached and it is turned to the far left again.
    if (position[0] == 7) {
        position[0] = 0;
        position[1] += 1;
    } else {
        position[0] += 1;
    }
}

float* Board::calculate_points(){
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (this->data[i][j] != nullptr) {
                Piece* temp = this->data[i][j];
                //call move functions of pieces.
                temp->move(this);
            }
        }
    }

    float* result = new float[2];
    result[0] = 0.0;
    result[1] = 0.0;

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (this->data[i][j] != nullptr) {
                Piece* temp = this->data[i][j];
                float current = temp->get_value();
                //If any piece is marked, break points.
                if (temp->check_marked()) {
                    current = current / 2;
                 }
                 if (temp->get_color() == 'b') {
                    result[0] += current;
                 } else {
                    result[1] += current;
                 }
            }
        }
    }
    return result;
}

bool Board::check_piece(int x, int y, char color) {
    if (x >= 0 && x < 8 && y >= 0 && y < 8) {
        //return false if there is a part at these coordinates.
        //The move functions of the pieces control the squares they can go with the
        //while loop one by one. If it encounters a stone, this while loop is closed.
        if (this->data[x][y] != nullptr) {
            Piece *temp = this->data[x][y];
            //if the moving piece color is different than
            //the temp piece, temp piece is marked.
            if (temp->get_color() != color) {
                temp->mark();
            }
            return false;
        } else {
            //If there is no piece in these coordinates, return true
            //to keep moving.
            return true;
        }
    } else {
        //Return false if these coordinates out of board.
        return false;
    }
}

