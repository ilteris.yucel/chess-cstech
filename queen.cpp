#include "queen.h"

Queen::Queen(char color, int* position)
:Piece(9, color, position){
}
QString Queen::to_string() {
    return QString("v%1").arg(this->get_color());
};
//Queen piece can go as far as it wants in horizontal and vertical directions and
//also in diagonal directions.
//It is equal as rook move + bishop move
void Queen::move(Board* board) {
    int* position = this->get_position();
    int* current_pos = new int[2];
    current_pos[0] = position[0];
    current_pos[1] = position[1];
    do {
        current_pos[0] -= 1;
    } while (board->check_piece(current_pos[0], current_pos[1], this->get_color()));

    current_pos[0] = position[0];
    do {
        current_pos[0] += 1;
    } while (board->check_piece(current_pos[0], current_pos[1], this->get_color()));

    current_pos[0] = position[0];
    do {
        current_pos[1] -= 1;
    } while (board->check_piece(current_pos[0], current_pos[1], this->get_color()));

    current_pos[1] = position[1];
    do {
        current_pos[1] += 1;
    } while (board->check_piece(current_pos[0], current_pos[1], this->get_color()));

    current_pos[1] = position[1];
    do {
        current_pos[0] -= 1;
        current_pos[1] -= 1;
    } while (board->check_piece(current_pos[0], current_pos[1], this->get_color()));

    current_pos[0] = position[0];
    current_pos[1] = position[1];
    do {
        current_pos[0] -= 1;
        current_pos[1] += 1;
    } while (board->check_piece(current_pos[0], current_pos[1], this->get_color()));

    current_pos[0] = position[0];
    current_pos[1] = position[1];
    do {
        current_pos[0] += 1;
        current_pos[1] -= 1;
    } while (board->check_piece(current_pos[0], current_pos[1], this->get_color()));

    current_pos[0] = position[0];
    current_pos[1] = position[1];
    do {
        current_pos[0] += 1;
        current_pos[1] += 1;
    } while (board->check_piece(current_pos[0], current_pos[1], this->get_color()));

    delete[] current_pos;
};
