# chess-cstech



## Getting started

This project is builded and runned on QTCreator with QMake file.
To build and run via command line manually follows these steps.

## Build
`cd <project-folder>`

`qmake cstech_chess.pro -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug`

`make -j8 in <your-build-folder>`

## Run

It is assumed that the input files will be read from the executable in a different build folder in the same folder as the project folder. 

For this reason, input files should be placed in the directory where the project and build folder are located.

![folder structure example](./folder-structure.png)

`cd <your-build-folder>`

`./cstech-chess <your-input.txt>`
