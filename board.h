#ifndef BOARD_H
#define BOARD_H
#include <utils.h>
#include <string>
//Since the Piece and Board classes include each other,
//we re-declare each other to avoid the compiler error.
class Piece;
#include "piece.h"
class Board {
private:
    Piece*** data;
    std::string filepath;
    void increment_position(int* position);
public:
    Board(std::string filepath);
    ~Board();
//    int** get_data();
    bool check_piece(int x, int y, char color);
    void read_file();
//    void to_string();

    //It calls the move functions of the pieces by traversing the entire board.
    //The movement positions of the pieces mark them as threatened
    //if there are opponent pieces in the positions they can go.
    //Finally, the whole board is visited once again and the score
    //is calculated according to the threat status of the pieces.
    float* calculate_points();

};

#endif // BOARD_H
