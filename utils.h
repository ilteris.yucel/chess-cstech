#ifndef UTILS_H
#define UTILS_H
#include <iostream>
#include <algorithm>
#include <QString>

//Function to copy two elements as array and push in
//2D array
static void fillArr(int** arr_t, int val, int val1, int index) {
    int arr[2] = {val, val1};
    std::copy(arr, arr + 2, arr_t[index]);
};

//static void printArr(int** arr_t, int row) {
//    for(int i = 0; i < row; i++)
//        std::cout << "[" << arr_t[i][0] << ", " << arr_t[i][1] << "]" << std::endl;
//};
#endif // UTILS_H
