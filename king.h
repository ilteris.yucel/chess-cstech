#ifndef KING_H
#define KING_H
#include "piece.h"

class King : public Piece {
public:
    King(char color, int* position);
    QString to_string() override;
    void move(Board* board) override;
};

#endif // KING_H
