#include "bishop.h"

Bishop::Bishop(char color, int* position)
 :Piece(3, color, position){

};
QString Bishop::to_string() {
    return QString("f%1").arg(this->get_color());
};
//bishop can go anywhere from the diagonals.
//The squares in the 4 diagonal directions were checked
//one by one with the do-while structure.
void Bishop::move(Board* board) {
    int* position = this->get_position();
    int* current_pos = new int[2];
    current_pos[0] = position[0];
    current_pos[1] = position[1];
    do {
        current_pos[0] -= 1;
        current_pos[1] -= 1;
    } while (board->check_piece(current_pos[0], current_pos[1], this->get_color()));

    current_pos[0] = position[0];
    current_pos[1] = position[1];
    do {
        current_pos[0] -= 1;
        current_pos[1] += 1;
    } while (board->check_piece(current_pos[0], current_pos[1], this->get_color()));

    current_pos[0] = position[0];
    current_pos[1] = position[1];
    do {
        current_pos[0] += 1;
        current_pos[1] -= 1;
    } while (board->check_piece(current_pos[0], current_pos[1], this->get_color()));

    current_pos[0] = position[0];
    current_pos[1] = position[1];
    do {
        current_pos[0] += 1;
        current_pos[1] += 1;
    } while (board->check_piece(current_pos[0], current_pos[1], this->get_color()));

    delete[] current_pos;
};

