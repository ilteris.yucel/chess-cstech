#ifndef ROOK_H
#define ROOK_H
#include "piece.h"


class Rook : public Piece
{
public:
    Rook(char color, int* position);
    QString to_string() override;
    void move(Board* board) override;
};

#endif // ROOK_H
