#include <QCoreApplication>
#include <board.h>
#include <QDir>
#include <QChar>

int main(int argc, char* argv[]) {
    QCoreApplication a(argc, argv);
    QChar ch = QDir::separator();
    QString filepath = QString("%1%2..%3%4").arg(QDir::currentPath(), ch, ch, argv[1]);
    std::cout << filepath.toStdString().c_str() << std::endl;
    Board* board = new Board(filepath.toStdString());
    board->read_file();

    float* result = board->calculate_points();
    std::cout << "White: " << result[0] << " Black " << result[1] << std::endl;
    return a.exec();
}
