#include "piece.h"


Piece::Piece(int value, char color, int* position) {
    this->value = value;
    this->color = color;
    this->position = position;
    this->marked = false;
};

Piece::~Piece() {
    //Heap memory is not used in there, but destructor is implemented
    //comseptually
    std::cout << "Destructor of piece is called" << std::endl;
    delete[] position;
};

bool Piece::change_color(char color) {
    //Check color value is valid
    if(color != 'b' || color != 'w') {
        std::cout << "Color value is invalid" << std::endl;
        return false;
    }
    this->color = color;
    return true;
};

int Piece::get_value() {
    return this->value;
};

char Piece::get_color() {
    return this->color;
};

int* Piece::get_position() {
    return this->position;
};

void Piece::mark() {
    this->marked = true;
};

void Piece::unmark() {
    this->marked = false;
};

bool Piece::check_marked(){
    return this->marked;
}

QString Piece::to_string() {
    return QString("p%1").arg(this->get_color());
};

void Piece::move(Board* board) {
    Q_UNUSED(board);
    return;
};
