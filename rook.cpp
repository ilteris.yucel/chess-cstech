#include "rook.h"

Rook::Rook(char color, int* position)
:Piece(5, color, position){

};
QString Rook::to_string() {
    return QString("k%1").arg(this->get_color());
};

//The rook piece can go as far as it wants in the horizontal and vertical directions.
void Rook::move(Board* board) {
    int* position = this->get_position();
    int* current_pos = new int[2];
    current_pos[0] = position[0];
    current_pos[1] = position[1];
    do {
        current_pos[0] -= 1;
    } while (board->check_piece(current_pos[0], current_pos[1], this->get_color()));

    current_pos[0] = position[0];
    do {
        current_pos[0] += 1;
    } while (board->check_piece(current_pos[0], current_pos[1], this->get_color()));

    current_pos[0] = position[0];
    do {
        current_pos[1] -= 1;
    } while (board->check_piece(current_pos[0], current_pos[1], this->get_color()));

    current_pos[1] = position[1];
    do {
        current_pos[1] += 1;
    } while (board->check_piece(current_pos[0], current_pos[1], this->get_color()));

    delete[] current_pos;
};

