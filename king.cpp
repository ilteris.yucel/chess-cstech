#include "king.h"

King::King(char color, int* position)
    :Piece(100, color, position){
}
QString King::to_string() {
    return QString("s%1").arg(this->get_color());
};
//The king moves one square in each direction.
//These squares are thrown into a 2D array and checked one by one with a for loop.
void King::move(Board* board) {
    int* position = this->get_position();
    int** checking = new int* [8];
    for(int i = 0; i < 8; i++)
        checking[i] = new int[2];
    fillArr(checking, position[0], position[1] - 1, 0);
    fillArr(checking, position[0], position[1] + 1, 1);
    fillArr(checking, position[0] - 1, position[1], 2);
    fillArr(checking, position[0] + 1, position[1], 3);
    fillArr(checking, position[0] - 1, position[1] - 1, 4);
    fillArr(checking, position[0] - 1, position[1] + 1, 5);
    fillArr(checking, position[0] + 1, position[1] - 1, 6);
    fillArr(checking, position[0] + 1, position[1] + 1, 7);
    for (int i = 0; i < 8; i++) {
        board->check_piece(checking[i][0], checking[i][1], this->get_color());
    }
};
