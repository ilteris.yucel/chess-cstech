#ifndef KNIGHT_H
#define KNIGHT_H
#include "piece.h"
#include "utils.h"

class Knight : public Piece {
public:
    Knight(char color, int* position);
    QString to_string() override;
    void move(Board* board) override;
};

#endif // KNIGHT_H
