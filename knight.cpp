#include "knight.h"

Knight::Knight(char color, int* position)
    :Piece(3, color, position){
}
QString Knight::to_string() {
    return QString("a%1").arg(this->get_color());
};
//The knight moves in an L-shape so he has 8 squares to go.
//These frames were kept in a separate 2D array
//and the check piece was called one by one with a for loop.
void Knight::move(Board* board) {
    int* position = this->get_position();
    int** checking = new int* [8];
    for(int i = 0; i < 8; i++)
        checking[i] = new int[2];
    fillArr(checking, position[0] - 1, position[1] - 2, 0);
    fillArr(checking, position[0] - 1, position[1] + 2, 1);
    fillArr(checking, position[0] - 2, position[1] - 1, 2);
    fillArr(checking, position[0] - 2, position[1] + 1, 3);
    fillArr(checking, position[0] + 1, position[1] - 2, 4);
    fillArr(checking, position[0] + 1, position[1] + 2, 5);
    fillArr(checking, position[0] + 2, position[1] - 1, 6);
    fillArr(checking, position[0] + 2, position[1] + 1, 7);
    for (int i = 0; i < 8; i++) {
        board->check_piece(checking[i][0], checking[i][1], this->get_color());
    }
    return;
};
