#ifndef QUEEN_H
#define QUEEN_H
#include "piece.h"

class Queen : public Piece
{
public:
    Queen(char color, int* position);
    QString to_string() override;
    void move(Board* board) override;
};

#endif // QUEEN_H
