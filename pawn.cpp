#include "pawn.h"

Pawn::Pawn(char color, int* position)
    :Piece(1, color, position){
}
QString Pawn::to_string() {
    return QString("p%1").arg(this->get_color());
};
//The pawn can only eat the stones diagonally.
//Since black starts from above the board and whites from below, the 2 cases are checked separately.
void Pawn::move(Board* board) {
    int* position = this->get_position();
    char color = this->get_color();
    if (color == 'b') {
        board->check_piece(position[0]-1,position[1]-1,color);
        board->check_piece(position[0]+1,position[1]-1,color);
    }
    if (color == 's') {
        board->check_piece(position[0]-1,position[1]+1,color);
        board->check_piece(position[0]+1,position[1]+1,color);
    }
};
