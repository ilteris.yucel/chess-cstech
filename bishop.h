#ifndef BISHOP_H
#define BISHOP_H
#include "piece.h"

class Bishop : public Piece
{
public:
    Bishop(char color, int* position);
    QString to_string() override;
    void move(Board* board) override;
};

#endif // BISHOP_H
